---
title: "RubyWorkout"
---
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div id="rw-explanation">
        <h2 id="rw-title">Welcome!</h2>
      </div>
      <div class="pull-right">
        <button type="button" id="btn_back"  class="btn btn-secondary"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Back</button>
        <button type="button" id="btn_next"  class="btn btn-secondary">Next <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div id="wrapper">
        <div id="editor_wrapper" class="col-lg-6">
          <h4>Text Editor</h4>
          <div id="editor" class="well" style="padding:0"></div>
          <div class="row">
            <div class="col-md-4">
              <button type="button" id="btn_run" class="btn btn-success btn-block">Run <span class="glyphicon glyphicon-play" aria-hidden="true"></span></button>
            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <h4>Code Output</h4>
          <div id="output" class="well" style="padding:0"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>Opal.load('rw');</script>
