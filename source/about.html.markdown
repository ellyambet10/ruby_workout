---
title: "RubyWorkout Disclaimer, Copyright, Contact"
date: 2019/18/07
author: EOA
---


### The MIT License

<div class="container-fluid">

<h4><i>
Copyright (C) 2019 EOA

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
</i></h4>

<h3>Credits</h3>

<h4><i>RubyWorkout version 1 by EOA</i></h4>

<h3>Source code</h3>

<h4><i>All source code for RubyWorkout (this website) is available on</i></h4>
<h4><i><a href="https://github.com/elibiz443/ruby_workout" target="_blank">GitHub</a>.</i></h4>

<h3>Contact</h3>

<h4><i>This website was created by EOA. I am a software developer. You can contact me via
<a href="https://twitter.com/OAmbet" target="_blank">Twitter</a>.</i></h4>
</div>
